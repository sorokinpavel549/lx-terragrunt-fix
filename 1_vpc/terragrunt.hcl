terraform {
  source = "git::https://github.com/cloudposse/terraform-aws-vpc.git//?ref=1.1.0"
}

inputs = { 
  ipv4_primary_cidr_block          = "10.0.0.0/16"
  assign_generated_ipv6_cidr_block = true
  ipv6_enabled                     = true  
  default_security_group_deny_all  = true
}

include "root" {
  path = find_in_parent_folders()
}