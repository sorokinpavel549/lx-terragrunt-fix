locals {
  aws_region = "eu-west-2"
}

remote_state {
  backend = "s3"
  generate = {
    path      = "_backend.tf"
    if_exists = "overwrite_terragrunt"
  }
  config = {
    bucket         = "lx-terragrunt-states-backend"
    key            = "${path_relative_to_include()}/terraform.tfstate"
    region         = local.aws_region
    encrypt        = true
    dynamodb_table = "lx-terragrunt-states-backend"
  }
}

generate "config" {
  path      = "_config.tf"
  if_exists = "overwrite"

  contents = <<EOF
provider "aws" {
   region = var.aws_region
 default_tags {
   tags = var.default_tags
  } 
}
variable "aws_region" {}
variable "default_tags" {
  type = map(string)
}
EOF
}


terraform {
  extra_arguments "common_vars" {
    commands = get_terraform_commands_that_need_vars()

    required_var_files = [
      find_in_parent_folders("common.tfvars"),
    ]
  }
}