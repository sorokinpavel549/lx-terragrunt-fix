1. You need to install [terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli), [terragrunt](https://terragrunt.gruntwork.io/docs/getting-started/install/) and [git](https://git-scm.com/downloads), and some code editor, for example [vscode](https://code.visualstudio.com/download) or [atom](https://atom.io/).

2. Configure environment variables in gitlab. Go to your project’s: Settings > CI/CD and expand the [Variables section](https://docs.gitlab.com/ee/ci/variables/). Add AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY, you can get then by following [instructions](https://docs.aws.amazon.com/keyspaces/latest/devguide/access.credentials.html). 

3. Clone repository, go to repository root and run command for create aws instance with configured vpc and security group:
```bash
terragrunt run-all apply -var-file="/path_to_repository/common.tfvars"
```
The creation of resources goes in this order 1_vpc > 2_subnets > 3_sg > 4_ec2 <br />
**1_vpc** - creates vpc, internet gateway. <br />
**2_subnets** - creates subnets, route tables. <br />
**3_sg**  - creates security groups. <br />
**4_ec2** - creates ec2 instances. <br />

**common.tfvars** - stores global variables. <br />

```bash
├── ec2
│   └── terragrunt.hcl
├── sg
│   └── terragrunt.hcl
├── subnets
│   └── terragrunt.hcl
└── vpc
│    └── terragrunt.hcl
├── common.tfvars
├── terragrunt.hcl
├── .gitlab-ci.yml
└── README.md
```

4. After push changes to repository will be trigger .gitlab-ci-yml. By this pipeline will check for changes in the infrastructure. They can be applied if necessary.

```bash
default:
  image: alpine/terragrunt


plan:
  stage: "test"
  script:
  - terragrunt validate-all
  - terragrunt plan-all -var-file=""$(pwd)"/common.tfvars"
  
  
apply:
  stage: "deploy"
  when: "manual"
  only: ["main"]
  script:
  - terragrunt apply-all -var-file=""$(pwd)"/common.tfvars" --terragrunt-non-interactive
```


5. As a result, you have a running aws instance with configured vpc and security group. Go to your [ec2 dashboard](https://console.aws.amazon.com/ec2/) and connect to your instance. For example, use the **ping** command to test the internet connection.