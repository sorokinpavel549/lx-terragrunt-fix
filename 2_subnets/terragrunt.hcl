terraform {
  source = "git::https://github.com/cloudposse/terraform-aws-dynamic-subnets.git//.?ref=2.0.2"
  
}

dependency "vpc" {
  config_path                             = "../1_vpc/"
  mock_outputs_allowed_terraform_commands = ["init", "validate", "plan", "terragrunt-info", "show"]
  mock_outputs = {
    vpc_id = "mock-output-vpc-sub"
    igw_id = "mock-output-igw"
  }
 }

inputs = {
  availability_zones     = ["eu-west-2a"]
  vpc_id                 = dependency.vpc.outputs.vpc_id
  igw_id                 = [dependency.vpc.outputs.igw_id]
  cidr_block             = "10.0.0.0/16"
  nat_gateway_enabled    = false
  public_subnets_enabled = true
  security_group_enabled = false
}

include "root" {
  path = find_in_parent_folders()
}