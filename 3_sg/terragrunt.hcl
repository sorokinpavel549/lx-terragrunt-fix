terraform {
  source = "git::https://github.com/cloudposse/terraform-aws-security-group.git//.?ref=1.0.1"
}

dependency "vpc" {
  config_path                             = "../1_vpc/"
  mock_outputs_allowed_terraform_commands = ["init", "validate", "plan", "terragrunt-info", "show"]
  mock_outputs = {
    vpc_id = "mock-output-vpc-sg"
  }
}

dependencies {
  paths = ["../2_subnets"]
}

inputs = {
  vpc_id  = dependency.vpc.outputs.vpc_id
  allow_all_egress = true
  rules = [
    {
      key         = "ssh"
      type        = "ingress"
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
      self        = null
      description = "Allow SSH from anywhere"
    },
    {
      key         = "HTTP"
      type        = "ingress"
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks = []
      self        = true
      description = "Allow HTTP from inside the security group"
    }
  ]
}

include "root" {
  path = find_in_parent_folders()
}
