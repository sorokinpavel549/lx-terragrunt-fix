  aws_region = "eu-west-2"

  name      = "sarokin"
  namespace = "lx"
  stage     = "prod"

  default_tags = {        
    "Team"             = "DevOps",
    "DeployedBy"       = "Terraform",
    "OwnerEmail"       = "pavel.sarokin@leverx.com"    
  }
 