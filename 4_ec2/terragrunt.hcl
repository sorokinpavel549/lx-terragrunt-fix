terraform {
  source = "git::https://github.com/cloudposse/terraform-aws-ec2-instance.git//.?ref=0.43.0"
}

dependency "vpc" {
  config_path                             = "../1_vpc/"
  mock_outputs_allowed_terraform_commands = ["init", "validate", "plan", "terragrunt-info", "show"]
  mock_outputs = {
    vpc_id = "mock-output-vpc-ec2"   
  }
}

dependency "subnets" {
  config_path                             = "../2_subnets/"
  mock_outputs_allowed_terraform_commands = ["init", "validate", "plan", "terragrunt-info", "show"]
  mock_outputs = {
    public_subnet_ids = ["mock-output-subnets"]

  }
}

dependency "sg" {
  config_path                             = "../3_sg/"
  mock_outputs_allowed_terraform_commands = ["init", "validate", "plan", "terragrunt-info", "show"]
  mock_outputs = {
    id = "mock-output-sg"

  }
}

inputs = { 
  ami                         = "ami-0fb391cce7a602d1f"
  ami_owner                   = "099720109477"
  instance_type               = "t2.nano" 
  ssh_key_pair                = "aws_key_sarokin"
  vpc_id                      = dependency.vpc.outputs.vpc_id
  security_groups             = [dependency.sg.outputs.id]
  subnet                      = dependency.subnets.outputs.public_subnet_ids[0]  
  associate_public_ip_address = true
  security_group_enabled      = false
 }

 include "root" {
  path   = find_in_parent_folders()
  expose = true
}
